{
  description = "My collection of flake templates";

  outputs = {self}: {
    templates = {
      basic = {
        path = ./basic;
        description = "Template for a basic flake";
      };
      pandoc = {
        path = ./pandoc;
        description = "Template for writing pandoc documents";
      };
      rust = {
        path = ./rust;
        description = "Rust template with naersk";
      };
      rust-nightly = {
        path = ./rust-nightly;
        description = "Rust nightly template with naersk and fenix";
      };
      ml = {
        path = ./ml;
        description = "Template for machine learning applications";
      };
      pony = {
        path = ./pony;
        description = "Template for programs written with Pony";
      };
    };

    defaultTemplate = self.templates.basic;
  };
}
