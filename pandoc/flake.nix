{
  description = "A document built with Pandoc";

  outputs = {
    self,
    nixpkgs,
  }: let
    document = fileName: outputType:
      pkgs.stdenv.mkDerivation {
        name = "document";
        src = ./.;
        buildInputs = with pkgs; [
          pandoc
          texlive.combined.scheme-small
        ];
        phases = ["unpackPhase" "buildPhase"];

        buildPhase = ''
          mkdir -p $out
          pandoc ${fileName} -o $out/${fileName}.${outputType}
        '';
      };
  in {
    packages.x86_64-linux.document = document "document.md" "pdf";
    defaultPackage.x86_64-linux = self.packages.x86_64-linux.document;
  };
}
