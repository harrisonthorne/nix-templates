{
  description = "a pony project!";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    utils,
  }: let
    appName = "app";
  in
    utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
      };

      nativeBuildInputs = with pkgs; [ponyc pony-corral pkg-config];
      buildInputs = with pkgs; [];
    in {
      # `nix develop`
      devShell = pkgs.mkShell {
        inherit nativeBuildInputs buildInputs;
      };
    });
}
